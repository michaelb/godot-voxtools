Misc thoughts on stuff to do in the form of scattered notes to self.

## Splitting up and renaming Dict3D

Presently it is a lot of things in one. It should be 3 classes, each
inheriting the one above:

1. Vox3D - simple 3D byte storage: Used for material storage.

2. ShapedVox3D - supports custom shapes: Useful for terrain data.

3. EntityShapedVox3D - supports floating entities: Useful for mixing
collideable entities with terrain data. (Physics engine)

## Optimization

First goal is to get it functionally complete, with a test suite.
Later, if performance is an issue, to possibly rewrite in C++.

Not sure if any of these would actually get any gains, just a few
random thoughts as I was writing this.

* have a precision-center, so that physics is all clumsy-block-based outside of
  a region near the Hero (still prevents enemies from falling off, etc, but
  could greatly computations of things like moving platforms, etc). Would be
  useful for simulations with lots of complexity which is often not in camera.

* Speed optimization by having side-entity chaining only be a last resort, a
  parallel entity `raw_array` that stores the ID (which is kept < 255) in all
  possible situations

* Another one: have the ENTITY bit + which relevant entities decay with a sweep
  "garbage collection" algo, so that it lazily updates stuff when things move,
  and only sweeps through and cleans up messes every so often. The main goal is
  to speed up ENTITY updates by keeping the bit sloppy, thus being more a "look
  out, an entity might be nearby" bit, as opposed to a "THERE IS DEFINITELY AN
  ENTITY NEARBY" bit.





