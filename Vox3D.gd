#
# Vox3D - simple 3D byte storage: Used for material storage.
#

var data
var array_size

var max_x = 0
var max_y = 0
var max_z = 0

#var out_of_bounds_warn = true
var out_of_bounds_warn = false
var out_of_bounds_value = 0 # EMPTY

func _init(max_x, max_y, max_z):
    _setup_vox3d(max_x, max_y, max_z)

func _setup_vox3d(max_x, max_y, max_z):
    self.max_x = max_x
    self.max_y = max_y
    self.max_z = max_z
    array_size = max_x * max_y * max_z
    # Create new array, and zero it out
    clear()

########################
# Clears entire contents
func clear():
    data = RawArray()
    data.resize(array_size)
    for i in range(array_size):
        data.set(i, 0) # EMPTY

#
# Given another instance Vox3D (presumably larger, e.g. the containing Vox3D),
# return an IntArray of indices that match up with this space, but in the
# other's space, at the given offset
func get_index_array_with_offset(other, x, y, z):
    if array_size > 200:
        print("Vox3D: WARNING: too big to get int array")
        return IntArray()
    var y_limit = max_y
    var x_limit = max_x
    var z_limit = max_z
    if floor(x) != x:
        x_limit += 1
    if floor(y) != y:
        y_limit += 1
    if floor(z) != z:
        z_limit += 1
    var indices = IntArray()
    indices.resize(y_limit * x_limit * z_limit)
    var i = 0
    for c_y in range(y_limit):
        for c_x in range(x_limit):
            for c_z in range(z_limit):
                var other_x = min(x + c_x, other.max_x - 1)
                var other_y = min(y + c_y, other.max_y - 1)
                var other_z = min(z + c_z, other.max_z - 1)
                # TODO: not sure why this is broken:
                #var other_x = x + c_x
                #var other_y = y + c_y
                #var other_z = z + c_z
                #if other_x >= other.max_x or other_y >= other.max_y or other_z >= other.max_z:
                #    continue
                var index = other.index_of(other_x, other_y, other_z)
                indices[i] = index
                i += 1
    return indices

#
# Returns array index of given x, y and z value
func index_of(x, y, z):
    return int(x) + (int(z) * max_x) + (int(y) * max_z * max_x)

#
# Sets byte given by value
func set(x, y, z, value):
    if x < 0 or y < 0 or z < 0 or x >= max_x or y >= max_y or z >= max_z:
        if out_of_bounds_warn:
            print("WARNING: Setting Vox3D out of bounds.")
        return
    data.set(index_of(x, y, z), value)

#
# Alias of set (DEPRECATED)
func init_block(x, y, z, value):
    set(x, y, z, value)

#
# Returns value at location cast as bool (DEPRECATED)
func check(x, y, z):
    return bool(get(x, y, z))

#
# Returns true if the given vector v is in bounds
func get(x, y, z):
    if x < 0 or y < 0 or z < 0 or x >= max_x or y >= max_y or z >= max_z:
        if out_of_bounds_warn:
            print("WARNING: Getting Vox3D out of bounds.")
        return out_of_bounds_value
    return data[index_of(x, y, z)]

#
# Out of bounds system, and warning
func set_out_of_bounds(value, warn):
    out_of_bounds_warn = warn
    out_of_bounds_value = value

#
# Returns true if the given vector v is in bounds
func vector_in_bounds(v):
    var x = v.x
    var y = v.y
    var z = v.z
    if x < 0 or y < 0 or z < 0 or x >= max_x or y >= max_y or z >= max_z:
        return false
    return true

#
# Copies from an array 
func copy_from(raw_array):
    if raw_array.size() != array_size:
        print("ERROR: Vox3D Copying array not the same size.")
        return
    for i in range(array_size):
        data.set(i, raw_array[i])

#
# Simply returns the value at location by the vector (DEPRECATED)
func get_by_vector(v):
    return get(v.x, v.y, v.z)

#
# Returns true if its exposed on any side (using 0 as exposed)
func is_exposed_by_vector(v):
    var val = get_by_vector(v)
    var sides = [
        Vector3(1,  0,  0),
        Vector3(0,  1,  0),
        Vector3(0,  0,  1),
        Vector3(-1, 0,  0),
        Vector3(0, -1,  0),
        Vector3(0,  0, -1),
    ]
    for side in sides:
        var val = get_by_vector(v + side)
        if val == 0: # EMPTY
            return true
    return false

