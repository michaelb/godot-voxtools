const ShapedVox3D = preload('./ShapedVox3D.gd')
const Vox3D = preload('./Vox3D.gd')

#
# Instantiates a ShapedVox3D from a dict keyed by Vector3 and with shape values
# as the values
static func from_v3_dict(v3_dict):
    var max_x = 1
    var max_y = 1
    var max_z = 1
    # First pass, determine dimensions
    for v3 in v3_dict:
        if v3.x < 0 or v3.y < 0 or v3.z < 0:
            print("ERROR: Vector3Array cannot have negative values")
        max_x = max(max_x, v3.x + 1)
        max_y = max(max_y, v3.y + 1)
        max_z = max(max_z, v3.z + 1)

    # Second pass, add values
    var new_d3d = ShapedVox3D.new(max_x, max_y, max_z)
    new_d3d.clear()
    for v3 in v3_dict:
        new_d3d.init_block(v3.x, v3.y, v3.z, v3_dict[v3])
    return new_d3d

#
# Instantiates a ShapedVox3D from an array like [[x, y, z] ...]
static func from_array(shape, coord_array):
    var v3array = Vector3Array()
    v3array.resize(coord_array.size())
    for c in coord_array:
        v3array.append(Vector3(c[0], c[1], c[2]))
    return from_v3_array(shape, v3array)

#
# Instantiates a ShapedVox3D from a Vector3 array using only given shape
static func from_v3_array(shape, v3_array):
    var dict = {}
    # just loop through adding each shape
    for v3 in v3_array:
        dict[v3] = shape
    return from_v3_dict(dict)


#
#  Given a path and dimensions, return a RawArray containing the files
#  dimensions
static func load_file(path, max_x, max_y, max_z):
    var array_size = max_x * max_y * max_z
    var f = File.new()
    var err = f.open(path, File.READ)
    if not f.is_open():
        print("VoxTools: ERROR: Can't open voxel data ", path)
        return
    var blocks = RawArray()
    blocks = f.get_buffer(array_size)
    f.close()
    return blocks

#
# Given a path and dimensions, construct and return either a ShapedVox3D or a
# Vox3D
static func from_file(is_shaped, path, max_x, max_y, max_z):
    var vox3d
    var raw_array = load_file(path, max_x, max_y, max_z)
    if is_shaped:
        vox3d = ShapedVox3D.new(max_x, max_y, max_z)
    else:
        vox3d = Vox3D.new(max_x, max_y, max_z)
    vox3d.copy_from(raw_array)
    return vox3d

#
#  Given a path and dimensions, return a RawArray containing the files
#  dimensions
static func save_to_file(path, vox3d):
    var f
    f = File.new()
    var err = f.open(path, File.WRITE)
    if not f.is_open():
        printerr("VoxTools: Can't open for saving ", path)
        return
    f.store_buffer(vox3d.data)
    f.close()


