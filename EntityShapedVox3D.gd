# EntityShapedVox3D - supports floating entities: Useful for mixing
# collide able entities with terrain data. (Physics engine)
#
# Implemented by flipping the ENTITY bit on all blocks that have overlap
# with an entity. Thus, entities moving quickly is slow.

# NOTE: Being re implemented different, no longer uses ENTITY bit, but
# parallel arrays

const ShapedVox3D = preload("./ShapedVox3D.gd")
const Consts = preload('./Consts.gd')
const Checkers = preload('./CheckerFunctions.gd')
const EMPTY = Consts.EMPTY

# Entity stored stuff
var entities_by_block_index = {}
var entity_by_id = {}
var entity_collision_dicts = {} # collision dicts for each entity
var entity_locations = {} # precise x, y, z locations
var next_entity_id = 1

# TODO: directly encode entity ID here, only falling to sparse dict if
# >255
# var vox3d

# Stores terrain
var svox3d
var vox3d

func _init(max_x, max_y, max_z):
    #vox3d = Vox3D.new(max_x, max_y, max_z)
    svox3d = ShapedVox3D.new(max_x, max_y, max_z)
    vox3d = svox3d.vox3d

func set_out_of_bounds(value, warn):
    svox3d.set_out_of_bounds(value, warn)

#
# Init block to given shape
func init_block(x, y, z, shape):
    svox3d.set(x, y, z, shape)

#
# Boolean check that uses collision shapes, for physics collision.
func check(x, y, z):
    var results = svox3d.check(x, y, z)
    if results:
        return true

    # Check entities instead
    return _get_or_check_entities(x, y, z, true)

#
# Returns any entities at location
func get_entities(x, y, z):
    return _get_or_check_entities(x, y, z, false)

func _get_or_check_entities(x, y, z, just_check):
    # just_check is true for getting a boolean (stopping after first
    # collision)
    var index = vox3d.index_of(x, y, z)

    # no collisions, for sure
    if not entities_by_block_index.has(index):
        if just_check:
            return false
        else:
            return []

    var entities = entities_by_block_index[index]
    var results = false
    if not just_check:
        results = []
    for entity_id in entities:
        var entity_offset = entity_locations[entity_id]
        # Get relative coords
        var r_x = x - entity_offset.x
        var r_y = y - entity_offset.y
        var r_z = z - entity_offset.z
        var collision_dict = entity_collision_dicts[entity_id]
        if collision_dict.check(r_x, r_y, r_z):
            # Check against relative coords
            if just_check:
                return true
            var entity = entity_by_id[entity_id]
            results.append(entity)
    return results

# NOTE: x, y, z can be floats
func add_entity(x, y, z, collision_dict, entity):
    var entity_id = next_entity_id
    entity_by_id[entity_id] = entity

    # Store precise entity offset
    entity_locations[entity_id] = Vector3(x, y, z)

    # Store collision infos
    entity_collision_dicts[entity_id] = collision_dict
    _add_entity_location(entity_id, x, y, z)
    next_entity_id += 1
    return entity_id

func update_entity_location(entity_id, x, y, z):
    var old_offset = entity_locations[entity_id]
    var new_offset = Vector3(x, y, z)
    entity_locations[entity_id] = new_offset # update offset
    var old_offset_was_even = old_offset.floor() == old_offset
    if new_offset.floor() == old_offset.floor() and not old_offset_was_even:
        # hasn't moved enough that we need to update the collision array
        return

    _remove_entity_location(entity_id, old_offset.x, old_offset.y, old_offset.z)
    _add_entity_location(entity_id, new_offset.x, new_offset.y, new_offset.z)
    # TODO: garbage collect entities_by_block_index arrays

func remove_entity(entity_id):
    var old_offset = entity_locations[entity_id]
    _remove_entity_location(entity_id, old_offset.x, old_offset.y, old_offset.z)
    entity_by_id.erase(entity_id)
    entity_locations.erase(entity_id)
    entity_collision_dicts.erase(entity_id)

func _add_entity_location(entity_id, x, y, z):
    var collision_dict = entity_collision_dicts[entity_id]
    for i in collision_dict.get_index_array_with_offset(self, x, y, z):
        # Add ENTITY bit to relevant block
        #vox3d.set(i, 1)

        # Add entity to "hash-chain"
        if not entities_by_block_index.has(i):
            entities_by_block_index[i] = []
        entities_by_block_index[i].append(entity_id)

func _remove_entity_location(entity_id, x, y, z):
    var collision_dict = entity_collision_dicts[entity_id]
    for i in collision_dict.get_index_array_with_offset(self, x, y, z):
        # Remove entity from "hash-chain"
        entities_by_block_index[i].erase(entity_id)
        # Remove ENTITY bit from relevant block
        #if entities_by_block_index[i].size() == 0:
        #    vox3d.set(i, 0) # remove ENTITY flag

