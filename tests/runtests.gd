extends SceneTree

func _init():
    load('./tests/unittest.gd').run([
        'tests.ShapedVox3D',
        'tests.EntityShapedVox3D',
        'tests.CheckerFunctions',
        'tests.Physics',
    ])
    quit()

